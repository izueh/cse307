import ply.yacc as yacc
import ply.lex as lex
from sys import argv

symbols = {}


class Node:
    def __init__(self):
        print("init node")

    def evaluate(self):
        return 0

    def execute(self):
        return 0


class NumberNode(Node):
    def __init__(self, v):
        if('.' in v):
            self.value = float(v)
        else:
            self.value = int(v)

    def evaluate(self):
        return self.value


class BooleanNode(Node):
    def __init__(self, v):
        self.value = v

    def evaluate(self):
        return self.value


class StringNode(Node):
    def __init__(self, v):
        self.val = v

    def evaluate(self):
        return self.val


class ListNode(Node):
    def __init__(self, v):
        self.value = v

    def evaluate(self):
        return self.value


class BooleanNone(Node):
    def __init__(self, v):
        self.value = v

    def evaluate(self):
        return self.value


class UnopNode(Node):
    def __init__(self, op, v):
        self.op = op
        self.val = v

    def evaluate(self):
        return not self.val.evaluate()


class AssignmentNode(Node):
    def __init__(self, x, y):
        self.name = x
        self.val = y

    def evaluate(self, x, y):
        symbols[x] = y


class BopNode(Node):

    ops = {
        '+': lambda x, y: x+y,
        '-': lambda x, y: x-y,
        '*': lambda x, y: x*y,
        '/': lambda x, y: x/y,
        '//': lambda x, y: x//y,
        '%': lambda x, y: x % y,
        '**': lambda x, y: x**y,
        '<': lambda x, y: x < y,
        '>': lambda x, y: x > y,
        '<=': lambda x, y: x <= y,
        '>=': lambda x, y: x >= y,
        '==': lambda x, y: x == y,
        '<>': lambda x, y: x != y,
        'and': lambda x, y: x and y,
        'or': lambda x, y: x or y,
        '[]': lambda x, y: x[y],
    }

    def __init__(self, op, v1, v2):
        self.v1 = v1
        self.v2 = v2
        self.op = op

    def evaluate(self):
        try:
            return BopNode.ops[self.op](self.v1.evaluate, self.v2.evaluate())
        except Exception:
            print('SEMANTIC ERROR')


class PrintNode(Node):
    def __init__(self, v):
        self.value = v

    def execute(self):
        self.value = self.value.evaluate()
        print(self.value)


class BlockNode(Node):
    def __init__(self, s):
        self.sl = [s]

    def execute(self):
        for statement in self.sl:
            statement.execute()


reserved = {
    'in': 'IN',
    'not': 'NOT',
    'and': 'AND',
    'or': 'OR',
    'True': 'BOOLEAN',
    'False': 'BOOLEAN'
}

tokens = list(set([
    'REAL',
    'INTEGER',
    'ID',
    'STRING',
    'PLUS',
    'MINUS',
    'TIMES',
    'MOD',
    'LT',
    'LE',
    'GT',
    'GE',
    'EQ',
    'NE',
    'ASSIGN',
    'POW',
    'LBRACE',
    'RBRACE',
    'COMMA',
    'FLOORDIVIDE',
    'DIVIDE',
    'LPAREN',
    'RPAREN',
    'NEWLINE',
]) | set(reserved.values()))


# Tokens
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_FLOORDIVIDE = r'//'
t_MOD = r'%'
t_POW = r'\*\*'

# logic
t_LT = r'<'
t_LE = r'<='
t_GT = r'>'
t_GE = r'>='
t_EQ = r'=='
t_NE = r'<>'

# control
t_LPAREN = r'\('
t_LBRACE = r'\['
t_LCBRACE = r'\{'
t_RPAREN = r'\)'
t_RBRACE = r'\]'
t_RCBRACE = r'\}'
t_COMMA = r','
t_ASSIGN = r'='
t_NEWLINE = r'\n'

bools = {'True': True, 'False': False}


def t_BOOLEAN(t):
    r'True|False'
    t.value = BooleanNode(bools[t.value])
    return t


def t_ID(t):
    r'_*[A-Za-z][A-Za-z0-9_]*'
    t.type = reserved.get(t.value, 'ID')
    return t


def t_NUMBER(t):
    r'-?\d*(\d\.|\.\d)\d* | \d+'
    try:
        t.value = NumberNode(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t


# Ignored characters
t_ignore = " \t"


def t_error(t):
    print("Syntax error at '%s'" % t.value)


# Build the lexer
lex.lex()

# Parsing rules


def p_block(t):
    """
    block : LCBRACE inblock RCBRACE
    """
    t[0] = t[2]


def p_inblock(t):
    """
    inblock : smt inblock
    """
    t[0] = t[2]
    t[0].sl.insert(0, t[1])


def p_inblock2(t):
    """
    inblock : smt
    """
    t[0] = BlockNode(t[1])


def p_smt(t):
    """
    smt : print_smt
        | assign
    """
    t[0] = t[1]


def p_print_smt(t):
    """
    print_smt : PRINT LPAREN expression RPAREN NEWLINE
    """
    t[0] = PrintNode(t[3])


def p_expr_smt(t):
    'smt: expression NEWLINE'
    t[0] = t[1]


def p_index(t):
    'expression : expression RBRACE expression LBRACE'
    t[0] = BopNode('[]', t[1], t[3])


def p_assign(t):
    'expression : expression ASSIGN expression'
    t[0] = AssignNode(t[1], t[2])


def p_expression_binop(t):
    '''expression : expression PLUS expression
                  | expression MINUS expression
                  | expression TIMES expression
                  | expression DIVIDE expression
                  | expression MOD expression
                  | expression POW expression
                  | expression AND expression
                  | expression OR expression
                  | expression LE expression
                  | expression GE expression
                  | expression LT expression
                  | expression GT expression
                  | expression EQ expression
                  | expression NE expression'''
    t[0] = BopNode(t[2], t[1], t[3])


def p_expression_unop(t):
    'expression : NOT expression'
    t[0] = UnopNode(t[1], t[2])


def p_empty(p):
    'empty :'
    pass


def p_list_comma(p):
    'list_type : expression COMMA list_type'
    p[0] = [p[1]] + p[3]


def p_list_expression(p):
    'list_type : expression'
    p[0] = [p[1]]


def p_list_empty(p):
    'list_type : empty'
    p[0] = []


def p_list(p):
    'list : LBRACE list_type RBRACE'
    p[0] = ListNode(p[2])


def p_expression_factor(t):
    '''expression : NUMBER
                  | STRING
                  | list
                  | BOOLEAN'''
    t[0] = t[1]


def p_error(t):
    if t.type != 'NEWLINE':
        while parser.token().type != 'NEWLINE':
            pass
    print("Syntax error at '%s'" % t.value)


precedence = (
    ('left', 'LBRACE'),
    ('left', 'LPAREN'),
    ('right', 'POW'),
    ('left', 'TIMES', 'DIVIDE', 'MOD', 'FLOORDIVIDE'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'IN'),
    ('left', 'NE', 'GE', 'GT', 'LE', 'LT', 'EQ'),
    ('left', 'NOT'),
    ('left', 'AND'),
    ('left', 'OR'),
    ('left', 'ASSIGN')
)

parser = yacc.yacc()

if __name__ == '__main__':
    with open(argv[1], 'r') as f:
        parser.parse(f.read)
