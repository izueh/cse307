import ply.lex as lex
import ply.yacc as yacc
from sys import argv

reserved = {
    'in': 'IN',
    'not': 'NOT',
    'and': 'AND',
    'or': 'OR',
    'True': 'BOOLEAN',
    'False': 'BOOLEAN'
}


tokens = list(set([
    'REAL',
    'INTEGER',
    'ID',
    'STRING',
    'PLUS',
    'MINUS',
    'TIMES',
    'MOD',
    'LT',
    'LE',
    'GT',
    'GE',
    'EQ',
    'NE',
    'POW',
    'LBRACE',
    'RBRACE',
    'COMMA',
    'FLOORDIVIDE',
    'DIVIDE',
    'LPAREN',
    'RPAREN',
    'NEWLINE',
]) | set(reserved.values()))


# operations
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_FLOORDIVIDE = r'//'
t_MOD = r'%'
t_POW = r'\*\*'

# logic
t_LT = r'<'
t_LE = r'<='
t_GT = r'>'
t_GE = r'>='
t_EQ = r'=='
t_NE = r'<>'

# control
t_LPAREN = r'\('
t_LBRACE = r'\['
t_RPAREN = r'\)'
t_RBRACE = r'\]'
t_COMMA = r','
t_SEMICOLON = r';'

bools = {'True': True, 'False': False}


def t_BOOLEAN(t):
    r'True|False'
    t.value = bools[t.value]
    return t


def t_ID(t):
    r'[A-Za-z][A-Za-z0-9_]*'
    t.type = reserved.get(t.value, 'ID')
    return t

# types


def t_STRING(t):
    r'".*?"|\'.*?\''
    t.value = t.value.replace('"', "'")
    return t


def t_REAL(t):
    r'\d*\.\d+'
    t.value = float(t.value)
    return t


def t_INTEGER(t):
    r'\d+'
    t.value = int(t.value)
    return t


t_ignore = ' \t\n'


def t_error(t):
    print('SYNTAX ERROR')
    if t.value[0] == '\n':
        t.lexer.skip(1)
        return
    count = 0
    while count < len(t.value) and t.value[count] != '\n':
        count += 1
    t.lexer.skip(count)


lexer = lex.lex()

precedence = (
    ('left', 'OR'),
    ('left', 'AND'),
    ('left', 'NOT'),
    ('left', 'NE', 'GE', 'GT', 'LE', 'LT', 'EQ'),
    ('left', 'IN'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'FLOORDIVIDE'),
    ('left', 'MOD'),
    ('left', 'TIMES', 'DIVIDE'),
    ('right', 'POW'),
    ('left', 'LBRACE', 'RBRACE'),
    ('left', 'LPAREN', 'RPAREN')
)


def p_block(p):
    'block : statement SEMICOLON block'
    p[0] = [p[1]] + p[3]


def p_block_newline(p):
    'block : NEWLINE block'
    p[0] = p[2]


def p_block_statement(p):
    'block : statement'
    p[0] = [p[1]]


def p_block_empty(p):
    'block : empty'
    p[0] = []


def p_statement_expression(p):
    'statement : expression'
    p[0] = p[1]
    if p[1] is not None:
        print(p[1])


def is_number(x):
    return type(x) == float or type(x) == int


def p_expression_paren(p):
    'expression : LPAREN expression RPAREN'
    p[0] = p[2]


def p_expression_index(p):
    'expression : expression LBRACE expression RBRACE'
    try:
        if type(p[1]) == str:
            p[1] = p[1][1:-1]
            p[0] = f"'{p[1][p[3]]}'"
        else:
            p[0] = p[1][p[3]]
    except Exception:
        print('SEMANTIC ERROR')
        raise SyntaxError


def p_expression_pow(p):
    'expression : expression POW expression'
    if not (is_number(p[1]) and is_number(p[3])):
        print('SEMANTIC ERROR')
        return
    p[0] = p[1] ** p[3]


def p_expression_times(p):
    'expression : expression TIMES expression'
    if not (is_number(p[1]) and is_number(p[3])):
        print('SEMANTIC ERROR')
        return
    p[0] = p[1] * p[3]


def p_expression_div(p):
    'expression : expression DIVIDE expression'
    if not (is_number(p[1]) and is_number(p[3])):
        print('SEMANTIC ERROR')
        return
    p[0] = p[1] / p[3]


def p_expression_mod(p):
    'expression : expression MOD expression'
    if not (is_number(p[1]) and is_number(p[3])):
        print('SEMANTIC ERROR')
        return
    p[0] = p[1] % p[3]


def p_expression_floordiv(p):
    'expression : expression FLOORDIVIDE expression'
    if not (is_number(p[1]) and is_number(p[3])):
        print('SEMANTIC ERROR')
        return
    p[0] = p[1] // p[3]


def p_expression_plus(p):
    'expression : expression PLUS expression'
    try:
        if type(p[1]) == str and type(p[3]) == str:
            p[0] = p[1][:-1] + p[3][1:]
        else:
            p[0] = p[1] + p[3]
    except Exception:
        print('SEMANTIC ERROR')
        raise SyntaxError


def p_expression_minus(p):
    'expression : expression MINUS expression'
    try:
        p[0] = p[1] - p[3]
    except Exception:
        print('SEMANTIC ERROR')
        raise SyntaxError


def p_expression_in(p):
    'expression : expression IN expression'
    try:
        if type(p[3]) == str:
            p[3] = p[3][1:-1]
        if type(p[1]) == str:
            p[1] = p[1][1:-1]
        if type(p[3]) == str or type(p[3]) == list:
            p[0] = p[1] in p[3]
        else:
            print('SEMANTIC ERROR')
    except Exception:
        print('SEMANTIC ERROR')
        raise SyntaxError


def p_expression_le(p):
    'expression : expression LE expression'
    p[0] = p[1] <= p[3]


def p_expression_ge(p):
    'expression : expression GE expression'
    p[0] = p[1] >= p[3]


def p_expression_lt(p):
    'expression : expression LT expression'
    p[0] = p[1] < p[3]


def p_expression_gt(p):
    'expression : expression GT expression'
    p[0] = p[1] > p[3]


def p_expression_eq(p):
    'expression : expression EQ expression'
    p[0] = p[1] == p[3]


def p_expression_ne(p):
    'expression : expression NE expression'
    p[0] = p[1] != p[3]


def p_expression_not(p):
    'expression : NOT expression'
    p[0] = not p[2]


def p_expression_and(p):
    'expression : expression AND expression'
    p[0] = p[1] and p[3]


def p_expression_or(p):
    'expression : expression OR expression'
    p[0] = p[1] or p[3]


def p_empty(p):
    'empty :'
    pass


def p_list_comma(p):
    'list_type : expression COMMA list_type'
    p[0] = [p[1]] + p[3]


def p_list_expression(p):
    'list_type : expression'
    p[0] = [p[1]]


def p_list_empty(p):
    'list_type : empty'
    p[0] = []


def p_list(p):
    'list : LBRACE list_type RBRACE'
    p[0] = p[2]


def p_number(p):
    '''number : REAL
    | INTEGER'''
    p[0] = p[1]


def p_expression_types(p):
    '''expression : number
    | STRING
    | list
    | BOOLEAN'''
    p[0] = p[1]


def p_error(p):
    count = 0
    if p and p.type != 'NEWLINE':
        while p.value < len(p.value) and p.value[count] != '\n':
            count += 1
        p.lexer.skip(count)
    print('SYNTAX ERROR')


# with open(argv[1], 'r') as f:
#     code = f.read()
#     lex.input(code)
#     while True:
#         token = lex.token()
#         if not token:
#             break
#         print(token)

parser = yacc.yacc()

with open(argv[1], 'r') as f:
    parser.parse(f.read())
