from collections import deque
from sys import argv


class Square:
    
    def __init__(self):
        self.val = None  
        self.row = 0 
        self.col = 0 
        self.avail = None 
    
    def __eq__(self, other):
        return len(self.avail) == len(other.avail)


    def __lt__(self, other):
        return len(self.avail) > len(other.avail)


    def __str__(self):
        return str(self.val)


class Solution:

    def __init__(self,row,col):
        self.grid = [[Square() for x in range(col+2)] for y in range(row+2)]
        self.to_do = [] 

    def __str__(self):
        _str = ''
        for i in range(1, len(self.grid)-1):
            for j in range(1, len(self.grid[i])-1):
                if j!=1:
                   _str+=' '
                _str += str(self.grid[i][j])
            _str+='\n'
        return _str


def adjacent_okay(s, val, r, c):
    for dr in range(-1,2):
        for dc in range(-1,2):
            if s.grid[r+dr][c+dc].val == val:
                return False
    return True


def attempt(s):
    if not s.to_do:
        return True
    curr = s.to_do.pop()
    row = curr.row
    col = curr.col
    avail = curr.avail

    for p in avail:
        if adjacent_okay(s, p, row, col):
            curr.avail.discard(p)
            s.grid[row][col].val = p
            if attempt(s):
                return True
            s.grid[row][col].val = '-' 
            curr.avail.add(p)
    s.to_do.append(curr)
    return False


def parse(f,row,col):
    s = Solution(row, col)
    for i in range(1,row+1):
        v = f.readline().strip().split(' ')
        for j in range(1,len(v)+1):
            s.grid[i][j].val = v[j-1] 
            s.grid[i][j].row = i
            s.grid[i][j].col= j

    no_regions = int(f.readline().strip())
    for i in range(no_regions):
        region = set([])
        v = f.readline().strip().split(' ')
        nsquares = int(v[0])
        v = v[1:]
        for n in range(1, nsquares+1):
            region.add(chr(ord('0')+n))
       
        for x in v:
            r = int(x[1])
            c = int(x[3])
            region.discard(s.grid[r][c].val)
            s.grid[r][c].avail = region
            if s.grid[r][c].val == '-':
                s.to_do.append(s.grid[r][c])

    return s 


if __name__ == '__main__':

    with open(argv[1], 'r') as f:
        p = int(f.readline().strip()) 
        for i in range(1,p+1):
            print(i)
            k, row, col = [int(x) for x in f.readline().strip().split(' ')] 
            soln = parse(f,row,col)
            if attempt(soln):
               print(str(soln).strip())
            else:
                print('No solution.')


