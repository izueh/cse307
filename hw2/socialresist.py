from math import fabs
from sys import argv


MAX_NODES=20
MAX_QUERIES=10
Adjacent = [[0.0 for j in range(MAX_NODES+MAX_QUERIES)]for i in range(MAX_NODES+1)]
query1 = [0 for i in range(0, MAX_QUERIES)]
query2 = [0 for i in range(0, MAX_QUERIES)]

def AdjacentInit(nnodes, nqueries):
    for i in range(0, nnodes+1):
        j=0
        while j < (nnodes + nqueries):
            Adjacent[i][j] = 0.0
            j+=1
    for i in range(0, nnodes):
        Adjacent[nnodes][i] = 1.0
    return 0

def ScanEdgeData(nnodes, pb):
    line = [int(x) for x in pb.strip().split()]
    node = line[0]
    count = line[1]
    Adjacent[node-1][node-1] += float(count)
    line = line[2:]
    for i in range(count):
        val = line[i]
        Adjacent[node-1][val-1] = -1.0
        Adjacent[val-1][node-1]=-1.0
        Adjacent[val-1][val-1]+=1.0
    return count


def FindMaxRow(nnodes, nqueries, currow):
    max = fabs(Adjacent[currow][currow])
    maxrow=currow
    for i in range(currow+1, nnodes+1):
        tmp = fabs(Adjacent[i][currow])
        if tmp > max:
            max = tmp
            maxrow = i
    return maxrow

def SwapRows(maxrow,currow,nnodes,nqueries):
    ncols = nnodes+nqueries
    for i in range(ncols):
        tmp = Adjacent[currow][i]
        Adjacent[currow][i] = Adjacent[maxrow][i]
        Adjacent[maxrow][i] = tmp

def Eliminate(currow, nrows, ncols):
    for i in range(nrows):
        if i == currow:
            continue
        factor = Adjacent[i][currow]
        for j in range(currow,ncols):
            Adjacent[i][j] -= factor*Adjacent[currow][j]
    return 0

def DumpMatrix(nrows, ncols):
    for i in range(nrows):
        for j in range(ncols):
            print(f'{Adjacent[i][j]:.2f} ', end='')
        print()

    print()


def SolveMatrix(nnodes, nqueries):
    ncols = nnodes + nqueries
    nrows = nnodes + 1
    for currow in range(nnodes):
        maxrow = FindMaxRow(nnodes,nqueries,currow)
        if maxrow != currow:
            SwapRows(maxrow,currow,nnodes,nqueries)
        pivot = Adjacent[currow][currow]
        if fabs(pivot) < 0.001:
            return -1
        pivot = 1.0/pivot
        for i in range(currow,ncols):
            Adjacent[currow][i] *= pivot
        Eliminate(currow,nrows,ncols)
    return 0

if __name__ == '__main__':

    with open(argv[1],'r') as f:
        nprob = int(f.readline().strip())
        for curprob in range(1, nprob+1):
            index, nnodes, nqueries, nedges = [int(x) for x in f.readline().strip().split(' ')]
            AdjacentInit(nnodes, nqueries)
            edgecnt =0
            edgelines =0
            while edgecnt < nedges:
                i = ScanEdgeData(nnodes, f.readline())  
                edgelines+=1
                edgecnt +=i
            for i in range(nqueries):
                queryno, query1[i], query2[i] = [int(x) for x in f.readline().strip().split(' ')]

                Adjacent[query1[i]-1][nnodes+i] = 1.0
                Adjacent[query2[i]-1][nnodes+i] = -1.0
            i = SolveMatrix(nnodes, nqueries)
            print(f'{curprob}',end='')
            for i in range(nqueries):
                dist = fabs(Adjacent[query1[i]-1][nnodes + i] - Adjacent[query2[i]-1][nnodes+i])
                print(f' {dist:.3f}', end='')
            print()


