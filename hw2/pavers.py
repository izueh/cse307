from math import fabs
from sys import stderr,argv

F=[0 for n in range(0,25+1)]
F1=[0 for n in range(0,25+1)]
F2=[0 for n in range(0,25+1)]
F3=[0 for n in range(0,25+1)]
G=[0 for n in range(0,25+1)]
G1=[0 for n in range(0,25+1)]
G2=[0 for n in range(0,25+1)]
G3=[0 for n in range(0,25+1)]
maxindex = 0

def comp_tiles():
    F[0] = 1
    F[1] = 2
    F[2] = 11
    F1[0] = 0
    F1[1] = 2
    F1[2] = 16
    F2[0] = 0
    F2[1] = 1
    F2[2] = 8
    F3[0] = 0
    F3[1] = 0
    F3[2] = 4
    G[0] = 0
    G[1] = 0
    G[2] = 2
    G1[0] =0
    G1[1] = 0
    G1[2] = 1
    G2[0] = 0
    G2[1] = 0
    G2[2] = 1
    G3[0] =0
    G3[1] = 0
    G3[2] = 1


    for n in range(2,25):
        F[n+1] = 2*F[n] + 7*F[n-1] + 4*G[n];
        F1[n+1] = 2*F1[n] + 2*F[n] + 7*F1[n-1] + 8*F[n-1] + 4*G1[n]+2*G[n];
        F2[n+1] = 2*F2[n] + F[n] + 7*F2[n-1] + 4*F[n-1] + 4*G2[n]+2*G[n];
        F3[n+1] = 2*F3[n] + 7*F3[n-1] + 4*F[n-1] + 4*G3[n]+2*G[n];
        test = 2.0* float((n+1)) *F[n+1]
        test1 = F1[n+1] + 2.0*F2[n+1] + 3.0*F3[n+1]
        if fabs(test - test1) > .0000001*test:
           print("mismatch", f=stderr)
        G[n+1] = 2*F[n-1] + G[n];
        G1[n+1] = 2*F1[n-1] + F[n-1] + G1[n];
        G2[n+1] = 2*F2[n-1] + F[n-1] + G2[n] + G[n];
        G3[n+1] = 2*F3[n-1] + F[n-1] + G3[n];

    return 0

if __name__ == '__main__':
    with open(argv[1],'r') as f:
        nprob = int(f.readline().strip())
        comp_tiles()
        for curprob in range(1,nprob+1):
            index, n =[int(x) for x in f.readline().strip().split(' ')]
            if n==1:
                print(f'{curprob} 2 2 1 0')
            else:
                print(f'{curprob} {F[n]} {F1[n]} {F2[n]} {F3[n]}')



